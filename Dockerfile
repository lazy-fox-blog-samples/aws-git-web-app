FROM node:12.18.2-stretch-slim as builder

ENV PORT 3000

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Installing dependencies
COPY package*.json /usr/src/app/
RUN npm install

# Copying source files
COPY . /usr/src/app

# Building app
RUN npm run build

FROM node:12.18.2-alpine3.11

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copy build results
COPY --from=builder /usr/src/app /usr/src/app

# Ports
EXPOSE 3000
# Running the app
CMD "npm" "run" "dev"
